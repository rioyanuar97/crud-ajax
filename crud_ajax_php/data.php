<table id="example" class="table table-striped table-bordered" style="width:100%">
    <thead>
        <tr>     
            <td>No</td>
            <td>Nama</td>
            <td>Alamat</td>
            <td>Telp</td>
            <td>Gender</td>
            <td>No_KTP</td>
            <td>Action</td>
        </tr>
    </thead>
    <tbody>
        <?php
            include 'koneksi.php';
            $no = 1;
            $query = "SELECT * FROM tb_karyawan";
            $coba1 = $db1->prepare($query);
            $coba1->execute();
            $res1 = $coba1->get_result();

            if ($res1->num_rows > 0) {
                while ($row = $res1->fetch_assoc()) {
                
                    $code = $row['code'];
                    $nama = $row['nama'];
                    $alamat = $row['alamat'];
                    $telp = $row['telp'];
                    $gender = $row['gender'];
                    $nomor_ktp = $row['nomor_ktp'];
        ?>
            <tr>
                <td><?php echo $no++; ?></td>
                <td><?php echo $nama; ?></td>
                <td><?php echo $alamat; ?></td>
                <td><?php echo $telp; ?></td>
                <td><?php echo $gender; ?></td>
                <td><?php echo $nomor_ktp; ?></td>
                <td>
                    <button id="<?php echo $code; ?>" class="btn btn-success btn-sm edit_data"> <i class="fa fa-edit"></i> Edit </button>
                    <button id="<?php echo $code; ?>" class="btn btn-danger btn-sm hapus_data"> <i class="fa fa-trash"></i> Hapus </button>
                </td>
            </tr>
        <?php } } else { ?> 
            <tr>
                <td colspan='7'>Tidak ada data ditemukan</td>
            </tr>
        <?php } ?>
    </tbody>
</table>


<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
    } );

    function reset(){
    document.getElementById("err_nama").innerHTML = "";
    document.getElementById("err_alamat").innerHTML = "";
    document.getElementById("err_no_ktp").innerHTML = "";
    document.getElementById("err_no_telp").innerHTML = "";
    document.getElementById("err_jenkel").innerHTML = "";
}
 
$(document).on('click', '.edit_data', function(){
    $('html, body').animate({ scrollTop: 0 }, 'slow');
    var code = $(this).attr('id');
    $.ajax({
        type: 'POST',
        url: "get_data.php",
        data: {code:code},
        dataType:'json',
        success: function(response) {
            reset();
            $('html, body').animate({ scrollTop: 30 }, 'slow');
            document.getElementById("code").value = response.code;
            document.getElementById("nama").value = response.nama;
            document.getElementById("nomor_ktp").value = response.nomor_ktp;
            document.getElementById("alamat").value = response.alamat;
            document.getElementById("telp").value = response.telp;
            if (response.gender=="Laki-laki") {
                document.getElementById("jenkel1").checked = true;
            } else {
                document.getElementById("jenkel2").checked = true;
            }
        }, error: function(response){
           console.log(response.responseText);
        }
    });
});

$(document).on('click', '.hapus_data', function(){
    var code = $(this).attr('id');
    $.ajax({
        type: 'POST',
        url: "hapus_data.php",
        data: {code:code},
        success: function() {
            $('.data').load("data.php");
        }, error: function(response){
            console.log(response.responseText);
        }
    });
});


</script>