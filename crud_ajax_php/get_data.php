<?php
session_start();
include 'koneksi.php';
include 'csrf.php';
 
$code = $_POST['code'];
$query = "SELECT code,nama,alamat,nomor_ktp,gender,telp FROM tb_karyawan WHERE code=?";
$dewan1 = $db1->prepare($query);
$dewan1->bind_param('i', $code);
$dewan1->execute();
$res1 = $dewan1->get_result();
while ($row = $res1->fetch_assoc()) {
    $h['code'] = $row["code"];
    $h['nama'] = $row["nama"];
    $h['alamat'] = $row["alamat"];
    $h['nomor_ktp'] = $row["nomor_ktp"];
    $h['gender'] = $row["gender"];
    $h['telp'] = $row["telp"];
}
echo json_encode($h);
 
$db1->close();
?>