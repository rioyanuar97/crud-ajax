<?php
session_start();
include 'koneksi.php';
include 'csrf.php';
 
$code = stripslashes(strip_tags(htmlspecialchars($_POST['code'] ,ENT_QUOTES)));
$nama = stripslashes(strip_tags(htmlspecialchars($_POST['nama'] ,ENT_QUOTES)));
$jenkel = stripslashes(strip_tags(htmlspecialchars($_POST['jenkel'] ,ENT_QUOTES)));
$alamat = stripslashes(strip_tags(htmlspecialchars($_POST['alamat'] ,ENT_QUOTES)));
$nomor_ktp = stripslashes(strip_tags(htmlspecialchars($_POST['nomor_ktp'] ,ENT_QUOTES)));
$telp = stripslashes(strip_tags(htmlspecialchars($_POST['telp'] ,ENT_QUOTES)));
 

if($code == ""){
    $query = "INSERT into tb_karyawan (nama, alamat, telp, gender, nomor_ktp) VALUES (?, ?, ?, ?, ?)";
    $dewan1 = $db1->prepare($query);
    $dewan1->bind_param("sssss", $nama, $alamat, $telp, $jenkel, $nomor_ktp);
    $dewan1->execute();
} else {
    $query = "UPDATE tb_karyawan SET nama=?, alamat=?, telp=?, gender=?, nomor_ktp=? WHERE code=?";
	$dewan1 = $db1->prepare($query);
	$dewan1->bind_param("sssssi", $nama, $alamat, $telp, $jenkel, $nomor_ktp, $code);
	$dewan1->execute();
}
 
echo json_encode(['success' => 'Sukses']);
 
$db1->close();
?>